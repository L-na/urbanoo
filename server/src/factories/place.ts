import { define } from 'typeorm-seeding';
import Faker from 'faker';
import Place from '../entities/place';

define(Place, (faker: typeof Faker) => {
  const place = new Place();
  place.googlePlaceId = faker.random.uuid();
  place.lat = faker.random.number();
  place.lng = faker.random.number();
  place.title = faker.random.word();
  place.description = faker.random.word();
  place.picture = faker.random.word();
  place.category = faker.random.word();
  return place;
});
