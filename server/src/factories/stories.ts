import { define } from 'typeorm-seeding';
import Faker from 'faker';
import Stories from '../entities/stories';

define(Stories, (faker: typeof Faker) => {
  const stories = new Stories();
  stories.name = faker.random.word();
  stories.description = faker.random.word();
  return stories;
});
