import { define, factory } from 'typeorm-seeding';
import Faker from 'faker';
import Journey from '../entities/journey';
import Step from '../entities/step';

define(Journey, (faker: typeof Faker) => {
  const journey = new Journey();
  journey.name = faker.random.word();
  journey.description = faker.random.word();
  journey.duration = faker.random.number();
  journey.difficulty = faker.random.number();
  journey.city = faker.random.word();
  journey.steps = factory(Step)().createMany(faker.random.number({
    min: 2,
    max: 10,
  })) as any;
  return journey;
});
