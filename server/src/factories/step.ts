import { define, factory } from 'typeorm-seeding';
import Faker from 'faker';
import Step from '../entities/step';
import Place from '../entities/place';

define(Step, (faker: typeof Faker) => {
  const step = new Step();
  step.number = faker.random.number();
  step.place = factory(Place)() as any;
  return step;
});
