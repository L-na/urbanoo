import HttpException from './httpException';

/**
 * Not found error
 */
export default class NotFoundError extends HttpException {
  constructor(id: string, message: string, stack?: string) {
    super(id, 'NOT_FOUND_ERROR', message, 404, stack);
  }
}
