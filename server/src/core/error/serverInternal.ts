import HttpException from './httpException';

/**
 * Server internal error
 */
export default class ServerInternalError extends HttpException {
  constructor(id: string, message: string, stack?: string) {
    super(id, 'INTERNAL_ERROR', message, 500, stack);
  }
}
