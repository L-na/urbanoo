import HttpException from './httpException';

/**
 * Bad request error
 */
export default class BadRequestError extends HttpException {
  constructor(id: string, message: string, stack?: string) {
    super(id, 'BAD_REQUEST', message, 400, stack);
  }
}
