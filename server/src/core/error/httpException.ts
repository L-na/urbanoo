/**
 * Default Http exception
 */
class HttpException extends Error {
    status: number;

    id: string;

    name: string;

    message: string;

    constructor(id: string, name :string, message: string, status: number, stack?: string) {
      super(message);
      this.name = name;
      this.status = status;
      this.message = message;
      this.stack = stack;
      this.id = id;
    }
}

/**
 * @swagger
 * definitions:
 *   HttpException:
 *     properties:
 *       id:
 *         type: string
 *       name:
 *         type: string
 *       message:
 *         type: string
 *       stack:
 *         type: string
 */

export default HttpException;
