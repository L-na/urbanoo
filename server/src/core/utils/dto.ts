import { IsNumberString } from 'class-validator';

export default class IdParamDTO {
    @IsNumberString()
    id: number;
}
