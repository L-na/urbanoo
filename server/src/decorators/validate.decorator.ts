import { validate, ValidationError } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { NextFunction, Response, Request } from 'express';
import BadRequestError from '../core/error/badRequest';

type descriptorType = TypedPropertyDescriptor<(
    req: Request,
     res: Response<any>,
     next: NextFunction
     ) => Promise<void>>

function validationFactory(metadataKey: Symbol, model: any, source: 'body' | 'params') {
  return (target: any, propertyName: string, descriptor: descriptorType): descriptorType => {
    Reflect.defineMetadata(metadataKey, model, target, propertyName);

    const method = descriptor.value;

    return {
      ...descriptor,
      async value(...args) {
        const innerModel = Reflect.getOwnMetadata(metadataKey, target, propertyName);

        const [req, res, next] = args;

        const plain = req[source];

        const parsedObject = plainToClass(innerModel, plain);

        let errors: ValidationError[] = [];

        try {
          errors = await validate(parsedObject);
        } catch (err) {
          console.log(err);
        }

        req[source] = parsedObject;

        if (errors.length > 0) {
          return next(new BadRequestError('VALIDATION', errors.toString()));
        }

        return method?.apply(this, [req, res, next]);
      },
    };
  };
}

export const ValidateParams = (dto: any) => validationFactory(Symbol('validate-params'), dto, 'params');
export const ValidateBody = (dto: any) => validationFactory(Symbol('validate-body'), dto, 'body');
