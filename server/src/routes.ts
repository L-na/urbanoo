import express from 'express';
import JourneyController from './controllers/journey';
import StoriesController from './controllers/stories';
import PlaceController from './controllers/place';
import StepController from './controllers/step';

export default class Routes {
  public journeyController: JourneyController = new JourneyController();

  public storiesController: StoriesController = new StoriesController();

  public placeController: PlaceController = new PlaceController();

  public stepController: StepController = new StepController();

  public routes(app: express.Application): void {
    // Journeys
    app.route('/journey').get(this.journeyController.getAll);
    app.route('/journey/last-five').get(this.journeyController.getLastFive);
    app.route('/journey/:id').get(this.journeyController.getOne);
    app.route('/journey').post(this.journeyController.addOne);
    app.route('/journey/:id').put(this.journeyController.editOne);
    app.route('/journey/:id').delete(this.journeyController.deleteOne);

    // Step
    app.route('/step').get(this.stepController.getAll);
    app.route('/step/:id').get(this.stepController.getOne);
    app.route('/step').post(this.stepController.addOne);
    app.route('/step/:id').put(this.stepController.editOne);
    app.route('/step/:id').delete(this.stepController.deleteOne);

    // Stories
    app.route('/stories').get(this.storiesController.getAll);
    app.route('/stories/:id').get(this.storiesController.getOne);
    app.route('/stories').post(this.storiesController.addOne);
    app.route('/stories/:id').put(this.storiesController.editOne);
    app.route('/stories/:id').delete(this.storiesController.deleteOne);

    // Places
    app.route('/place').get(this.placeController.getAll);
    app.route('/place/:id').get(this.placeController.getOne);
    app.route('/place').post(this.placeController.addOne);
    app.route('/place/:id').put(this.placeController.editOne);
    app.route('/place/:id').delete(this.placeController.deleteOne);
  }
}
