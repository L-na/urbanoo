import { createConnection } from 'typeorm';
import App from './app';
import { API_PORT } from './config';

createConnection().then(async () => {
  const { app } = new App();
  app.listen(API_PORT, () => {
    console.log(`Express server listening on port ${API_PORT}`);
  });
}).catch((error) => {
  console.log(`Unable to launch server: ${error}`);
});
