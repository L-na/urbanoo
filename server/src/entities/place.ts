import {
  IsInt, IsNotEmpty, IsNumber, IsOptional, IsString,
} from 'class-validator';
import {
  Entity, Column, PrimaryGeneratedColumn, OneToMany,
} from 'typeorm';
import Step from './step';

/**
 * @swagger
 * definitions:
 *   Place:
 *     properties:
 *       id:
 *         type: number
 *       googlePlaceId:
 *         type: string
 *       lat:
 *         type: number
 *       lng:
 *         type: number
 *       title:
 *         type: string
 *       description:
 *         type: string
 *       picture:
 *         type: string
 *       category:
 *         type: string
 *       steps:
 *         type: array
 *         items: {
 *          $ref: '#/definitions/Step'
 *         }
 */

@Entity()
export default class Place {
    @IsInt()
    @IsOptional()
    @PrimaryGeneratedColumn()
    id: number;

    @IsNotEmpty()
    @IsString()
    @Column('character varying', {
      length: 300,
      nullable: true,
    })
    googlePlaceId: string;

    @IsNotEmpty()
    @IsNumber()
    @Column('float')
    lat: number;

    @IsNotEmpty()
    @IsNumber()
    @Column('float')
    lng: number;

    @IsNotEmpty()
    @IsString()
    @Column('character varying', {
      length: 100,
    })
    title: string;

    @IsNotEmpty()
    @IsString()
    @Column('character varying', {
      length: 300,
    })
    description: string;

    @IsNotEmpty()
    @IsString()
    @Column('character varying', {
      length: 100,
    })
    picture: string;

    @IsNotEmpty()
    @IsString()
    @Column('character varying', {
      length: 50,
    })
    category: string;

    @OneToMany(() => Step, (step: Step) => step.place)
    steps: Step[];
}
