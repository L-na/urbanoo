import {
  IsInt, IsNotEmpty, IsOptional, IsString,
} from 'class-validator';
import {
  Entity, Column, PrimaryGeneratedColumn, OneToMany,
} from 'typeorm';
import Step from './step';

/**
 * @swagger
 * definitions:
 *   Journey:
 *     properties:
 *       id:
 *         type: number
 *       name:
 *         type: string
 *       description:
 *         type: string
 *       duration:
 *         type: number
 *       difficulty:
 *         type: number
 *       city:
 *         type: string
 *       steps:
 *          type: array
 *          items: {
 *           $ref: '#/definitions/Step'
 *          }
 */
@Entity()
export default class Journey {
    @IsOptional()
    @IsInt()
    @PrimaryGeneratedColumn()
    id: number;

    @IsNotEmpty()
    @IsString()
    @Column('character varying', {
      length: 100,
    })
    name: string;

    @IsNotEmpty()
    @IsString()
    @Column('text')
    description: string;

    @IsNotEmpty()
    @IsInt()
    @Column('int')
    duration: number;

    @IsNotEmpty()
    @IsInt()
    @Column('int')
    difficulty: number;

    @IsNotEmpty()
    @IsString()
    @Column('character varying', {
      length: 100,
    })
    city: string;

    @OneToMany(() => Step, (step: Step) => step.journey)
    steps: Step[];
}
