import { IsInt, IsNotEmpty, IsOptional } from 'class-validator';
import {
  Entity, Column, PrimaryGeneratedColumn, ManyToOne,
} from 'typeorm';
import Journey from './journey';
import Place from './place';

/**
 * @swagger
 * definitions:
 *   Step:
 *     properties:
 *       id:
 *         type: number
 *       number:
 *         type: number
 *       journey:
 *         type: Object
 *         $ref: '#/definitions/Journey'
 *       place:
 *         type: Object
 *         $ref: '#/definitions/Place'
 */
@Entity()
export default class Step {
    @IsOptional()
    @IsInt()
    @PrimaryGeneratedColumn()
    id: number;

    @IsNotEmpty()
    @IsInt()
    @Column('int')
    number: number;

    @ManyToOne(() => Journey, (journey: Journey) => journey.steps)
    journey: Journey;

    @ManyToOne(() => Place, (place: Place) => place.steps)
    place: Place;
}
