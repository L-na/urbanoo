import {
  IsInt, IsNotEmpty, IsOptional, IsString,
} from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

/**
 * @swagger
 * definitions:
 *   Stories:
 *     properties:
 *       id:
 *         type: number
 *       name:
 *         type: string
 *       description:
 *         type: string
 */
@Entity()
export default class Stories {
    @IsOptional()
    @IsInt()
    @PrimaryGeneratedColumn()
    id: number;

    @IsNotEmpty()
    @IsString()
    @Column('character varying', {
      length: 100,
    })
    name: string;

    @IsNotEmpty()
    @IsString()
    @Column('text')
    description: string;
}
