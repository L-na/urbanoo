import dotenv from 'dotenv';

dotenv.config();

export const API_PREFIX = process.env.API_PREFIX || '/';
export const { API_PORT } = process.env;
