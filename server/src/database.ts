import { createConnection } from 'typeorm';

export default () => {
  createConnection().then(() => {
    console.log('Connected to database');
  }).catch((error) => console.log(error));
};
