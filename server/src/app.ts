import express, {
  Response, Request,
} from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import path from 'path';
import { createStream } from 'rotating-file-stream';
import cors, { CorsOptions, CorsOptionsDelegate } from 'cors';
import swaggerUi from 'swagger-ui-express';
import Routes from './routes';
import * as specs from './swagger';
import HttpException from './core/error/httpException';
import 'reflect-metadata';

class App {
  public app: express.Application;

  public routePrv: Routes = new Routes();

  constructor() {
    this.app = express();
    this.config();
    this.routePrv.routes(this.app);
    this.errorHandling();
  }

  private errorHandling(): void {
    this.app.use(
      (error: HttpException, request: Request, response: Response) => {
        response.status(error.status).json({
          id: error.id,
          name: error.name,
          message: error.message,
        });
      },
    );
  }

  private config(): void {
    const enableOriginsForCredentials: CorsOptionsDelegate = (req: express.Request,
      callback: (err: Error | null, options?: CorsOptions) => void) => {
      callback(null, {
        origin: true,
        credentials: true,
      });
    };

    // create a rotating write stream
    const accessLogStream = createStream('server.log', {
      interval: '1d', // rotate daily
      path: path.join(__dirname, 'log'),
    });

    this.app.use('/docs', swaggerUi.serve, swaggerUi.setup(specs.default));

    const logger = morgan('dev');

    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(cookieParser());
    this.app.use(morgan('combined', { stream: accessLogStream }));
    this.app.use(logger);
    this.app.use(cors(enableOriginsForCredentials));
  }
}

export default App;
