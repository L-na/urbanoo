import { MigrationInterface, QueryRunner } from 'typeorm';

export default class Init1613121760704 implements MigrationInterface {
    name = 'Init1613121760704'

    public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query('CREATE TABLE "place" ("id" SERIAL NOT NULL, "googlePlaceId" character varying(300), "lat" double precision NOT NULL, "lng" double precision NOT NULL, "title" character varying(100) NOT NULL, "description" character varying(300) NOT NULL, "picture" character varying(100) NOT NULL, "category" character varying(50) NOT NULL, CONSTRAINT "PK_96ab91d43aa89c5de1b59ee7cca" PRIMARY KEY ("id"))');
      await queryRunner.query('CREATE TABLE "step" ("id" SERIAL NOT NULL, "number" integer NOT NULL, "journeyId" integer, "placeId" integer, CONSTRAINT "PK_70d386ace569c3d265e05db0cc7" PRIMARY KEY ("id"))');
      await queryRunner.query('CREATE TABLE "journey" ("id" SERIAL NOT NULL, "name" character varying(100) NOT NULL, "description" text NOT NULL, "duration" integer NOT NULL, "difficulty" integer NOT NULL, "city" character varying(100) NOT NULL, CONSTRAINT "PK_0dfc23b6e61590ef493cf3adcde" PRIMARY KEY ("id"))');
      await queryRunner.query('CREATE TABLE "stories" ("id" SERIAL NOT NULL, "name" character varying(100) NOT NULL, "description" text NOT NULL, CONSTRAINT "PK_bb6f880b260ed96c452b32a39f0" PRIMARY KEY ("id"))');
      await queryRunner.query('ALTER TABLE "step" ADD CONSTRAINT "FK_71d98b993ed2b9a22cc31a3a312" FOREIGN KEY ("journeyId") REFERENCES "journey"("id") ON DELETE NO ACTION ON UPDATE NO ACTION');
      await queryRunner.query('ALTER TABLE "step" ADD CONSTRAINT "FK_09369f6993ba94219ab4e927520" FOREIGN KEY ("placeId") REFERENCES "place"("id") ON DELETE NO ACTION ON UPDATE NO ACTION');
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.query('ALTER TABLE "step" DROP CONSTRAINT "FK_09369f6993ba94219ab4e927520"');
      await queryRunner.query('ALTER TABLE "step" DROP CONSTRAINT "FK_71d98b993ed2b9a22cc31a3a312"');
      await queryRunner.query('DROP TABLE "stories"');
      await queryRunner.query('DROP TABLE "journey"');
      await queryRunner.query('DROP TABLE "step"');
      await queryRunner.query('DROP TABLE "place"');
    }
}
