import { Factory, Seeder } from 'typeorm-seeding';
import Journey from '../entities/journey';

export default class CreateJourney implements Seeder {
  public async run(factory: Factory): Promise<any> {
    await factory(Journey)().createMany(10);
  }
}
