import { Factory, Seeder } from 'typeorm-seeding';
import Stories from '../entities/stories';

export default class CreateStories implements Seeder {
  public async run(factory: Factory): Promise<any> {
    await factory(Stories)().createMany(10);
  }
}
