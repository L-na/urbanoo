import { NextFunction, Request, Response } from 'express';
import { getRepository } from 'typeorm';
import BadRequestError from '../core/error/badRequest';
import NotFoundError from '../core/error/notFound';
import ServerInternalError from '../core/error/serverInternal';
import IdParamDTO from '../core/utils/dto';
import { ValidateBody, ValidateParams } from '../decorators/validate.decorator';
import Step from '../entities/step';

/**
 * Step controller
 */
export default class StepController {
/**
 * @swagger
 * /step:
 *    get:
 *      tags:
 *          - Step
 *      summary: this return all steps.
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      responses:
 *        200:
 *          description: Steps
 *          schema:
 *           type: array
 *           items: {
 *            $ref: '#/definitions/Step'
 *           }
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
  async getAll(req: Request, res: Response, next: NextFunction): Promise<any> {
    try {
      const steps = await getRepository(Step).find();

      return res.status(200).send(steps);
    } catch (err) {
      return next(new ServerInternalError('GET_ALL_STEP', err.message, err.stack));
    }
  }

  /**
 * @swagger
 * /step/{id}:
 *    get:
 *      tags:
 *          - Step
 *      summary: this return a specific step
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: id
 *         in: path
 *         description: Step id
 *         required: true
 *         type: integer
 *      responses:
 *        200:
 *          description: Step
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/Step'
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 *        404:
 *          description: Not found
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
 @ValidateParams(IdParamDTO)
  async getOne(req: Request, res: Response, next: NextFunction): Promise<any> {
    const id: number = parseInt(req.params.id, 10);

    try {
      const step = await getRepository(Step).findOneOrFail(id);
      return res.status(200).send(step);
    } catch (err) {
      if (err.name === 'EntityNotFound') return next(new NotFoundError('GET_ONE_STEP', err.message, err.stack));
      return next(new ServerInternalError('GET_ONE_STEP', err.message, err.stack));
    }
  }

 /**
 * @swagger
 * /step:
 *    post:
 *      tags:
 *          - Step
 *      summary: this create a step
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: step
 *         in: formData
 *         description: New step
 *         required: true
 *         schema:
 *          $ref: '#/definitions/Step'
 *      responses:
 *        201:
 *          description: Step
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/Step'
 *        404:
 *          description: Not found
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
 @ValidateBody(Step)
 async addOne(req: Request, res: Response, next: NextFunction): Promise<any> {
   const step: Step = req.body;

   if (undefined !== step.journey && undefined !== step.place) {
     try {
       const createdStep = await getRepository(Step).save(step);

       return res.status(201).send(createdStep);
     } catch (err) {
       if (err.code === '23503') return next(new NotFoundError('ADD_ONE_STEP', err.statusMessage, err.stack));
       return next(new ServerInternalError('ADD_ONE_STEP', err.statusMessage, err.stack));
     }
   } else {
     return next(new BadRequestError('EDIT_ONE_STEP', "Journey or Place does'nt exist in body"));
   }
 }

 /**
 * @swagger
 * /step/{id}:
 *    put:
 *      tags:
 *          - Step
 *      summary: this update a step
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: id
 *         in: path
 *         description: Id of step to update
 *         required: true
 *         type: integer
 *       - name: step
 *         in: formData
 *         description: New step
 *         required: true
 *         schema:
 *          $ref: '#/definitions/Step'
 *      responses:
 *        200:
 *          description: Step
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/Step'
 *        404:
 *          description: Not found
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
 @ValidateParams(IdParamDTO)
 @ValidateBody(Step)
 async editOne(req: Request, res: Response, next: NextFunction): Promise<any> {
   const id: number = parseInt(req.params.id, 10);
   const step: Step = req.body;

   if (undefined !== step.journey && undefined !== step.place) {
     try {
       await getRepository(Step).findOneOrFail(id, { select: ['id'] });
       const createdStep = await getRepository(Step).save({ ...step, id });

       return res.status(200).send(createdStep);
     } catch (err) {
       if (err.name === 'EntityNotFound') return next(new NotFoundError('EDIT_ONE_STEP', err.statusMessage, err.stack));
       return next(new ServerInternalError('EDIT_ONE_STEP', err.statusMessage, err.stack));
     }
   } else {
     return next(new BadRequestError('EDIT_ONE_STEP', "Journey or Place does'nt exist in body"));
   }
 }

 /**
 * @swagger
 * /step/{id}:
 *    delete:
 *      tags:
 *          - Step
 *      summary: this delete a specific step
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: id
 *         in: path
 *         description: Step id
 *         required: true
 *         type: integer
 *      responses:
 *        204:
 *          description: Step
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/Step'
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
 @ValidateParams(IdParamDTO)
 async deleteOne(req: Request, res: Response, next: NextFunction): Promise<any> {
   const id: number = parseInt(req.params.id, 10);

   try {
     const deleteResult = await getRepository(Step).delete(id);

     return res.status(204).send(deleteResult);
   } catch (err) {
     return next(new ServerInternalError('DELETE_ONE_STEP', err.message, err.stack));
   }
 }
}
