import { NextFunction, Request, Response } from 'express';
import { getRepository } from 'typeorm';
import NotFoundError from '../core/error/notFound';
import ServerInternalError from '../core/error/serverInternal';
import IdParamDTO from '../core/utils/dto';
import { ValidateBody, ValidateParams } from '../decorators/validate.decorator';
import Place from '../entities/place';

/**
 * Place controller
 */
export default class PlaceController {
/**
 * @swagger
 * /place:
 *    get:
 *      tags:
 *          - Place
 *      summary: this return all  places.
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: latitude
 *         in: path
 *         description: Min latitude of the area
 *         required: false
 *         type: number
 *       - name: longitude
 *         in: path
 *         description: Min longitude of the area
 *         required: false
 *         type: number
 *       - name: latitudeDelta
 *         in: path
 *         description: Latitude delta used to compute max of area
 *         required: false
 *         type: number
 *       - name: longitudeDelta
 *         in: path
 *         description: longitude delta used to compute max of area
 *         required: false
 *         type: number
 *      responses:
 *        200:
 *          description: Places
 *          schema:
 *           type: array
 *           items: {
 *            $ref: '#/definitions/Place'
 *           }
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
  async getAll(req: Request, res: Response, next: NextFunction): Promise<any> {
    const {
      latitude,
      longitude,
      latitudeDelta,
      longitudeDelta,
    }: any = req.query;

    try {
      const queryBuilder = getRepository(Place)
        .createQueryBuilder('place');

      if (latitude !== undefined
        && longitude !== undefined
        && latitudeDelta !== undefined
        && longitudeDelta !== undefined) {
        queryBuilder.where('place.lat > :minLat', { minLat: (parseFloat(latitude) - parseFloat(latitudeDelta)) })
          .andWhere('place.lat < :maxLat', { maxLat: (parseFloat(latitude) + parseFloat(latitudeDelta)) })
          .andWhere('place.lng > :minLng', { minLng: (parseFloat(longitude) - parseFloat(longitudeDelta)) })
          .andWhere('place.lng < :maxLng', { maxLng: (parseFloat(longitude) + parseFloat(longitudeDelta)) });
      }

      return res.status(200).send(await queryBuilder.getMany());
    } catch (err) {
      return next(new ServerInternalError('GET_ALL_PLACE', err.message, err.stack));
    }
  }

  /**
 * @swagger
 * /place/{id}:
 *    get:
 *      tags:
 *          - Place
 *      summary: this return a specific place
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      responses:
 *        200:
 *          description: Place
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/Place'
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 *        404:
 *          description: Not found error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
 @ValidateParams(IdParamDTO)
  async getOne(req: Request, res: Response, next: NextFunction): Promise<any> {
    const id: number = parseInt(req.params.id, 10);

    try {
      const place = await getRepository(Place).findOneOrFail(id);

      return res.status(200).send(place);
    } catch (err) {
      if (err.name === 'EntityNotFound') return next(new NotFoundError('GET_ONE_PLACE', err.message, err.stack));
      return next(new ServerInternalError('GET_ONE_PLACE', err.message, err.stack));
    }
  }

 /**
 * @swagger
 * /place:
 *    post:
 *      tags:
 *          - Place
 *      summary: this delete a place.
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: path
 *         in: formData
 *         description: New path
 *         required: true
 *         schema:
 *          $ref: '#/definitions/Place'
 *      responses:
 *        201:
 *          description: Place
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/Place'
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
 @ValidateBody(Place)
 async addOne(req: Request, res: Response, next: NextFunction): Promise<any> {
   const place: Place = req.body;

   try {
     const addedPlace = await getRepository(Place).save(place);

     return res.status(201).send(addedPlace);
   } catch (err) {
     return next(new ServerInternalError('ADD_ONE_PLACE', err.statusMessage, err.stack));
   }
 }

 /**
 * @swagger
 * /place/{id}:
 *    put:
 *      tags:
 *          - Place
 *      summary: this update a specific  place
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: id
 *         in: path
 *         description: Id of path to update
 *         required: true
 *         type: integer
 *       - name: place
 *         in: formData
 *         description: New place
 *         required: true
 *         schema:
 *          $ref: '#/definitions/Place'
 *      responses:
 *        200:
 *          description: Place
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/Place'
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 *        404:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
 @ValidateParams(IdParamDTO)
 @ValidateBody(Place)
 async editOne(req: Request, res: Response, next: NextFunction): Promise<any> {
   const place: Place = req.body;

   const id: number = parseInt(req.params.id, 10);

   try {
     await getRepository(Place).findOneOrFail(id, { select: ['id'] });
     const updatedPlace = await getRepository(Place).save({ ...place, id });

     return res.status(200).send(updatedPlace);
   } catch (err) {
     if (err.name === 'EntityNotFound') return next(new NotFoundError('EDIT_ONE_PLACE', err.statusMessage, err.stack));
     return next(new ServerInternalError('EDIT_ONE_PLACE', err.statusMessage, err.stack));
   }
 }

 /**
 * @swagger
 * /place/{id}:
 *    delete:
 *      tags:
 *          - Place
 *      summary: this delete a specific  place
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      responses:
 *        204:
 *          description: Place
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/Place'
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
 @ValidateParams(IdParamDTO)
 async deleteOne(req: Request, res: Response, next: NextFunction): Promise<any> {
   const id: number = parseInt(req.params.id, 10);

   try {
     const deleteResult = await getRepository(Place).delete(id);

     return res.status(204).send(deleteResult);
   } catch (err) {
     return next(new ServerInternalError('DELETE_ONE_PLACE', err.message, err.stack));
   }
 }
}
