import { NextFunction, Request, Response } from 'express';
import { getConnection, getRepository } from 'typeorm';
import Journey from '../entities/journey';
import Step from '../entities/step';
import Place from '../entities/place';
import ServerInternalError from '../core/error/serverInternal';
import NotFoundError from '../core/error/notFound';
import BadRequestError from '../core/error/badRequest';
import { ValidateBody, ValidateParams } from '../decorators/validate.decorator';
import IdParamDTO from '../core/utils/dto';

/**
 * Journey controller
 */
export default class JourneyController {
  /**
     * @swagger
     * /journey:
     *    get:
     *      tags:
     *          - Journey
     *      summary: this return all journey.
     *      produces:
     *       - application/json
     *      parameters:
     *       - name: googlePlaceId
     *         in: path
     *         description: Id of Google Place
     *         required: false
     *         type: string
     *      responses:
     *        200:
     *          description: Journey
     *          schema:
     *           type: array
     *           items: {
     *            $ref: '#/definitions/Journey'
     *           }
     *        500:
     *          description: error
     *          schema:
     *           type: Object
     *           $ref: '#/definitions/HttpException'
     */
  async getAll(req: Request, res: Response, next: NextFunction): Promise<any> {
    const { googlePlaceId } = req.query;

    try {
      const journeysQueryBuilder = getRepository(Journey).createQueryBuilder('journey').select(['journey']);

      if (googlePlaceId) {
        journeysQueryBuilder.leftJoinAndSelect(Step, 'step', 'step."journeyId" = journey.id')
          .leftJoinAndSelect(Place, 'place', 'step."placeId" = place.id')
          .where('place.googlePlaceId = :googlePlaceId', { googlePlaceId });
      }

      return res.status(200).send(await journeysQueryBuilder.getMany());
    } catch (err) {
      return next(new ServerInternalError('GET_ALL_JOURNEY', err.message, err.stack));
    }
  }

    /**
     * @swagger
     * /journey/{id}:
     *    get:
     *      tags:
     *          - Journey
     *      summary: this return a specific journey.
     *      produces:
     *       - application/json
     *      parameters:
     *       - name: id
     *         in: path
     *         description: Journey id
     *         required: true
     *         type: integer
     *      responses:
     *        200:
     *          description: Journey
     *          schema:
     *           type: Object
     *           $ref: '#/definitions/Journey'
     *        500:
     *          description: error
     *          schema:
     *           type: Object
     *           $ref: '#/definitions/HttpException'
     *        404:
     *          description: error
     *          schema:
     *           type: Object
     *           $ref: '#/definitions/HttpException'
     */
    @ValidateParams(IdParamDTO)
  async getOne(req: Request, res: Response, next: NextFunction): Promise<any> {
    const id: number = parseInt(req.params.id, 10);

    try {
      const journey = await getRepository(Journey).findOneOrFail(id, { relations: ['steps', 'steps.place'] });
      return res.status(200).send(journey);
    } catch (err) {
      if (err.name === 'EntityNotFound') return next(new NotFoundError('GET_ONE_JOURNEY', err.message, err.stack));
      return next(new ServerInternalError('GET_ONE_JOURNEY', err.message, err.stack));
    }
  }

    /**
     * @swagger
     * /journey:
     *    post:
     *      tags:
     *          - Journey
     *      summary: this create a journey.
     *      consumes:
     *       - application/json
     *      produces:
     *       - application/json
     *      parameters:
     *       - name: journey
     *         in: formData
     *         description: New journey
     *         required: true
     *         schema:
     *          $ref: '#/definitions/Journey'
     *      responses:
     *        201:
     *          description: Journey
     *          schema:
     *           type: Object
     *           $ref: '#/definitions/Journey'
     *        500:
     *          description: error
     *          schema:
     *           type: Object
     *           $ref: '#/definitions/HttpException'
     *        404:
     *          description: Not found error
     *          schema:
     *           type: Object
     *           $ref: '#/definitions/HttpException'
     */
    @ValidateBody(Journey)
    async addOne(req: Request, res: Response, next: NextFunction): Promise<any> {
      const journey: Journey = req.body;

      const queryRunner = getConnection().createQueryRunner();
      await queryRunner.connect();
      await queryRunner.startTransaction();

      try {
        const createdJourney = await queryRunner.manager.getRepository(Journey)
          .save({ ...journey, steps: undefined });

        if (journey.steps && journey.steps.length > 0) {
          const createdSteps = await queryRunner.manager.getRepository(Step).save(journey.steps);
          await queryRunner.manager.getRepository(Journey)
            .createQueryBuilder()
            .relation(Journey, 'steps')
            .of(createdJourney)
            .add(createdSteps);
        }

        await queryRunner.commitTransaction();

        return res.status(201).send(createdJourney);
      } catch (err) {
        await queryRunner.rollbackTransaction();

        if (err.code === '23503') return next(new NotFoundError('ADD_ONE_JOURNEY', err.statusMessage, err.stack));
        return next(new ServerInternalError('ADD_ONE_JOURNEY', err.statusMessage, err.stack));
      } finally {
        await queryRunner.release();
      }
    }

    /**
     * @swagger
     * /journey/{id}:
     *    put:
     *      tags:
     *       - Journey
     *      summary: this update a journey.
     *      consumes:
     *       - application/json
     *      produces:
     *       - application/json
     *      parameters:
     *       - name: id
     *         in: path
     *         description: Id of journey to update
     *         required: true
     *         type: integer
     *       - name: journey
     *         in: formData
     *         description: New journey
     *         required: true
     *         schema:
     *          $ref: '#/definitions/Journey'
     *      responses:
     *        200:
     *          description: Journey
     *          schema:
     *           type: Object
     *           $ref: '#/definitions/Journey'
     *        500:
     *          description: error
     *          schema:
     *           type: Object
     *           $ref: '#/definitions/HttpException'
     *        404:
     *          description: Not found
     *          schema:
     *           type: Object
     *           $ref: '#/definitions/HttpException'
     */
    @ValidateBody(Journey)
    @ValidateParams(IdParamDTO)
    async editOne(req: Request, res: Response, next: NextFunction): Promise<any> {
      const journey: Journey = req.body;

      const id: number = parseInt(req.params.id, 10);

      if (!journey) return next(new BadRequestError('EDIT_ONE_JOURNEY', 'No journey given'));

      try {
        await getRepository(Journey).findOneOrFail(id, { select: ['id'] });
        journey.id = id;
        const updatedJourney = await getRepository(Journey).save(journey);

        return res.status(200).send(updatedJourney);
      } catch (err) {
        if (err.name === 'EntityNotFound') return next(new NotFoundError('EDIT_ONE_JOURNEY', err.statusMessage, err.stack));
        return next(new ServerInternalError('EDIT_ONE_JOURNEY', err.statusMessage, err.stack));
      }
    }

    /**
     * @swagger
     * /journey:
     *    delete:
     *      tags:
     *       - Journey
     *      summary: this delete a journey.
     *      consumes:
     *       - application/json
     *      produces:
     *       - application/json
     *      parameters:
     *       - name: id
     *         in: path
     *         description: Id of journey to delete
     *         required: true
     *         type: integer
     *      responses:
     *        204:
     *          description: Delete Journey result
     *        500:
     *          description: error
     *          schema:
     *           type: Object
     *           $ref: '#/definitions/HttpException'
     */
    @ValidateParams(IdParamDTO)
    async deleteOne(req: Request, res: Response, next: NextFunction): Promise<any> {
      const id: number = parseInt(req.params.id, 10);

      try {
        const deleteResult = await getRepository(Journey).delete(id);

        return res.status(204).send(deleteResult);
      } catch (err) {
        return next(new ServerInternalError('DELETE_ONE_JOURNEY', err.message, err.stack));
      }
    }

    /**
     * @swagger
     * /journey/last-five:
     *    get:
     *      tags:
     *          - Journey
     *      summary: this return the last five journey.
     *      produces:
     *       - application/json
     *      responses:
     *        200:
     *          description: Journey
     *        500:
     *          description: error
     *          schema:
     *           type: Object
     *           $ref: '#/definitions/HttpException'
     */
    async getLastFive(req: Request, res: Response, next: NextFunction): Promise<any> {
      try {
        const journeysQueryBuilder = getRepository(Journey).createQueryBuilder('journey').select(['journey']).orderBy("journey.id", "DESC").limit(5);
        return res.status(200).send(await journeysQueryBuilder.getMany());
      } catch (err) {
        return next(new ServerInternalError('GET_ALL_JOURNEY', err.message, err.stack));
      }
    }
  
}
