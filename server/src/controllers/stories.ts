import { NextFunction, Request, Response } from 'express';
import { getRepository } from 'typeorm';
import NotFoundError from '../core/error/notFound';
import ServerInternalError from '../core/error/serverInternal';
import IdParamDTO from '../core/utils/dto';
import { ValidateBody, ValidateParams } from '../decorators/validate.decorator';
import Stories from '../entities/stories';

/**
 * Stories controller
 */
export default class StoriesController {
/**
 * @swagger
 * /stories:
 *    get:
 *      tags:
 *          - Stories
 *      summary: this return all stories.
 *      produces:
 *       - application/json
 *      responses:
 *        200:
 *          description: Stories
 *          schema:
 *           type: array
 *           items: {
 *            $ref: '#/definitions/Stories'
 *           }
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
  async getAll(req: Request, res: Response, next: NextFunction): Promise<any> {
    try {
      const stories = await getRepository(Stories).find();

      return res.status(200).send(stories);
    } catch (err) {
      return next(new ServerInternalError('GET_ALL_STORIES', err.message, err.stack));
    }
  }

  /**
 * @swagger
 * /stories/{id}:
 *    get:
 *      tags:
 *          - Stories
 *      summary: this return a specific story.
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: id
 *         in: path
 *         description: Story id
 *         required: true
 *         type: integer
 *      responses:
 *        200:
 *          description: Stories
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/Stories'
 *        404:
 *          description: Not found
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
 @ValidateParams(IdParamDTO)
  async getOne(req: Request, res: Response, next: NextFunction): Promise<any> {
    const id: number = parseInt(req.params.id, 10);

    try {
      const story = await getRepository(Stories).findOneOrFail(id);

      return res.status(200).send(story);
    } catch (err) {
      if (err.name === 'EntityNotFound') return next(new NotFoundError('GET_ONE_STORY', err.statusMessage, err.stack));
      return next(new ServerInternalError('GET_ONE_STORY', err.message, err.stack));
    }
  }

 /**
 * @swagger
 * /stories:
 *    post:
 *      tags:
 *          - Stories
 *      summary: this create a story.
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: story
 *         in: formData
 *         description: New story
 *         required: true
 *         schema:
 *          $ref: '#/definitions/Stories'
 *      responses:
 *        201:
 *          description: Stories
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/Stories'
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
 @ValidateBody(Stories)
 async addOne(req: Request, res: Response, next: NextFunction): Promise<any> {
   const story: Stories = req.body;

   try {
     const createdStory = await getRepository(Stories).save(story);

     return res.status(201).send(createdStory);
   } catch (err) {
     if (err.code === '23503') return next(new NotFoundError('ADD_ONE_STORY', err.statusMessage, err.stack));
     return next(new ServerInternalError('ADD_ONE_STORY', err.message, err.stack));
   }
 }

 /**
 * @swagger
 * /stories/{id}:
 *    put:
 *      tags:
 *          - Stories
 *      summary: this modify a story.
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: story
 *         in: formData
 *         description: New story
 *         required: true
 *         schema:
 *          $ref: '#/definitions/Stories'
 *       - name: id
 *         in: path
 *         description: Story id
 *         required: true
 *         type: integer
 *      responses:
 *        200:
 *          description: Stories
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/Stories'
 *        404:
 *          description: Not found
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
 @ValidateBody(Stories)
 @ValidateParams(IdParamDTO)
 async editOne(req: Request, res: Response, next: NextFunction): Promise<any> {
   const story: Stories = req.body;
   const id: number = parseInt(req.params.id, 10);

   try {
     await getRepository(Stories).findOneOrFail(id, { select: ['id'] });
     const editedStory = await getRepository(Stories).save({ ...story, id });

     return res.status(200).send(editedStory);
   } catch (err) {
     if (err.name === 'EntityNotFound') return next(new NotFoundError('EDIT_ONE_STORY', err.message, err.stack));
     return next(new ServerInternalError('EDIT_ONE_STORY', err.message, err.stack));
   }
 }

 /**
 * @swagger
 * /stories/{id}:
 *    delete:
 *      tags:
 *          - Stories
 *      summary: this delete a story.
 *      consumes:
 *       - application/json
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: id
 *         in: path
 *         description: Story id
 *         required: true
 *         type: integer
 *      responses:
 *        204:
 *          description: Stories
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/Stories'
 *        500:
 *          description: error
 *          schema:
 *           type: Object
 *           $ref: '#/definitions/HttpException'
 */
 @ValidateParams(IdParamDTO)
 async deleteOne(req: Request, res: Response, next: NextFunction): Promise<any> {
   const id: number = parseInt(req.params.id, 10);

   try {
     const deleteResult = await getRepository(Stories).delete(id);

     return res.status(204).send(deleteResult);
   } catch (err) {
     return next(new ServerInternalError('DELETE_ONE_STORY', err.message, err.stack));
   }
 }
}
