import swaggerJsdoc from 'swagger-jsdoc';
import { API_PREFIX } from './config';

const options = {
  openapi: '3.0.0',
  apis: ['**/*.ts'],
  swaggerDefinition: {
    basePath: API_PREFIX,
    info: {
      description: 'Documentation de l\'api du projet Urbanoo.',
      title: 'Urbanoo API',
      version: '0.0.1',
    },
  },
};

const specs = swaggerJsdoc(options);

/**
 * @swagger
 * definitions:
 *   Response:
 *     properties:
 *       message:
 *         type: string
 */

export default specs;
