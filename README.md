# Urbanoo
App that suggest and allow to create journeys with differents steps to a place.

## Commits

```
[INIT] Set new service
[FEAT] Create a new functionnality
[IMPR] Improvement on a feature
[REFACTO] Refactoring a feature
[FIX] Fix an error
[DOC] Documentation, comments
```

## Branches
```
[master] This is the main branch
[feature-branch] For every new feature
```

## Start server
First you need to create a .env inside the db and server directories.

```
cp ./db/.env.example ./db/.env
cp ./server/.env.example ./server/.env
```

Don't forget to add your personnal values inside.

Then to start the server use :
```
docker-compose up
```
After modifiying server dependencies you will need to build the image again :

```
docker-compose up --build -V
```

Then to create the database, you will need to run the migrations :
```
npm run migration:run
```

And for development purpose, you will maybe need some test data, so run :
```
npm run seed:run
```

In case you already have a postgres running on your compouter, don't forget to kill all port already in use :
```
sudo lsof -i :5432
sudo kill {pid}
```