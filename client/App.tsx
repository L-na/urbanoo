import React, {useState, useRef} from 'react';
import Router from './src/Router';
import { MainContext } from "./src/context/MainContext";
import Toast, {DURATION} from 'react-native-easy-toast'

export default () => {
  const toast: any = useRef();

  const showToast = (message: string) => {
    if (toast.current) {
      toast.current.show(message)
    }
  }

    return (<MainContext.Provider value={{showToast}}>
      <Router></Router>
      <Toast ref={toast}/>
    </MainContext.Provider>);


}