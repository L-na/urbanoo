// React components
import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

// Screens import
import HomeScreen from "./screens/HomeScreen";
import ResearchScreen from "./screens/ResearchScreen";
import MapScreen from "./screens/MapScreen";
import CircuitScreen from "./screens/CircuitScreen";
import CircuitInfosScreen from "./screens/CircuitInfosScreen";

const Stack = createStackNavigator();

export default function Router() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" options={{ headerShown: false }} component={HomeScreen} />
        <Stack.Screen name="Research" options={{ headerShown: false }} component={ResearchScreen} />
        <Stack.Screen name="Map" options={{ headerShown: false }} component={MapScreen} />
        <Stack.Screen name="Circuit" options={{ headerShown: false }} component={CircuitScreen} />
        <Stack.Screen name="CircuitInfos" options={{ headerShown: false }} component={CircuitInfosScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

