import React from "react";
import { Dimensions, TouchableOpacity } from "react-native";
import { StyleSheet, View, ImageBackground, Text } from "react-native";
import Svg, { Ellipse } from "react-native-svg";
import EntypoIcon from "react-native-vector-icons/Entypo";
import { NavigationProp, useNavigation, useRoute, RouteProp } from '@react-navigation/native';
import SimpleLineIconsIcon from "react-native-vector-icons/SimpleLineIcons";
import { Helpers } from '../utils/helpers';
import Icon from "react-native-vector-icons/FontAwesome";
import theme from "../Theme";

interface Props {
  navigation: NavigationProp<any>,
  route: RouteProp<any, any>
}

interface State {
  isDown: Boolean,
  details: {
    city?: String,
    description?: String,
    difficulty?: Number,
    duration?: Number,
    id?: Number,
    name?: String,
  },
  duration?: String
}

class MapDetailComponent extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      isDown: false,
      details: {},
      duration: null,
    }
  }

    componentDidMount() {
    try {
      const data = this.props.route.params.data;
      if( null == data || undefined == data ) {
        return;
      }

      const toMin = Helpers.millisToMinutesAndSeconds(data.duration)
      this.setState({
        details: data,
        duration: toMin,
      });
    } catch (e) {
      console.log(e);
    }
  }

  dropDown = () => {
    console.log("drop down")
    this.setState({
      isDown: true
    }, ()=>console.log(this.state.isDown))
  }

  dropUp = () => {
    console.log("drop up")
    this.setState({
      isDown: false
    }, ()=>console.log(this.state.isDown))
  }

  render() {
    const { isDown, details, duration } = this.state;

    return (
      <View style={styles.container}>
        { (false == isDown) ?
          <TouchableOpacity onPress={this.dropDown} style={[styles.buttonArrow, {top: 5, marginTop: 10, marginBottom: 15}]}>
              <EntypoIcon  name="chevron-down" style={styles.iconArrow}></EntypoIcon>
          </TouchableOpacity> :
          <TouchableOpacity onPress={this.dropUp} style={[styles.buttonArrow, {bottom: 0, backgroundColor: "#FFFFEE", borderRadius: 25, marginBottom: 25}]}>
              <EntypoIcon  name="chevron-up" style={styles.iconArrow}></EntypoIcon>
          </TouchableOpacity>
        }

        <View style={[styles.containerCard, (true == isDown) ? styles.dropDown : styles.dropUp]}>
          <View style={styles.containerFlex}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("CircuitInfos",{
                  data: details
              })}>
              <View style={styles.containerDetail}>
                <ImageBackground
                    source={require("../assets/images/imageEx.png")}
                    resizeMode="cover"
                    style={[styles.image, (isDown == true) ? {opacity: 0} : {opacity: 1}]}
                ></ImageBackground>

                <View style={[styles.filter, (isDown == true) ? {opacity: 0} : {opacity: 1}]}>
                    <View style={styles.light}></View>
                    <View style={styles.gradientDark}></View>
                </View>
              
                <View style={[styles.infos, (isDown == true) ? {opacity: 0} : {opacity: 1}]}>
                  <Text style={styles.title}>{details.name}</Text>
                  <View style={styles.infosRow}>
                      <View style={[styles.infosDetails]}>
                          <SimpleLineIconsIcon
                              name="clock"
                              style={styles.iconTime}
                          ></SimpleLineIconsIcon>
                          <Text style={styles.time}> {(null == duration) ? "" : duration} </Text>
                      </View>
                      <View style={[styles.infosDetails]}>
                        <Icon name="tachometer" style={[styles.icon, {marginLeft: 50}]}></Icon>
                        <Text style={styles.iconEnv}> {details.difficulty}</Text>
                      </View>
                  </View>
                </View>

              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    zIndex: 1,
    width: Dimensions.get('window').width,
    height: 300,
    borderRadius: 8,
    position: "absolute",
    bottom: 0,
  },
  containerCard: {
    zIndex: 1,
    marginTop: 15,
    width: Dimensions.get('window').width,
    height: 250,
    backgroundColor: "#FFFFFE",
    borderRadius: 25,
  },
  dropDown: {
    height: 0
  },
  dropUp: {
    height: 300
  },
  icon: {
    marginRight: 5,
    color: theme.primary,
    zIndex: 999,
  },
  buttonArrow: {
    zIndex: 999,
    right: 0,
    position: "absolute",
    marginRight: (Dimensions.get('window').width - 310)/2,
    color: theme.primary,
    fontSize: 36,
    height: 50,
    width: 50,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconArrow: {
    color: "rgba(100,180,20,1)",
    fontSize: 36,
  },
  containerFlex: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "center",
  },
  containerDetail: {
    width: 310,
    height: 211
  },
  image: {
    zIndex: 998, // under the drop button
    width: 310,
    height: 211,
    marginTop: 5,
    position: "absolute",
    borderRadius: 8,
    overflow: "hidden"
  },
  filter: {
    width: 310,
    height: 211,
    //backgroundColor: "rgba(250,238,231,0.17)",
    borderRadius: 8
  },
  infos: {
    width: 310,
    position:"absolute",
    bottom: 0,
    marginBottom: 20
  },
  light: {
    backgroundColor: theme.white,
    width: 310,
    height: 144,
    // zIndex: 998,
  },
  gradientDark: {
    backgroundColor: "rgba(74,63,54,0.65)",
    // shadowColor: theme.brown,
    // shadowOffset: {
    //   width: 0,
    //   height: -18
    // },
    // elevation: 30,
    // shadowOpacity: 0.8,
    // shadowRadius: 10,
    width: 310,
    height: 73,
    borderRadius: 8,
    // opacity: 0.8,
    zIndex: 998,
  },
  infosRow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  infosDetails: {
    flexDirection: "row",
    alignItems: "center"
  },
  title: {
    //fontFamily: "roboto-700",
    color: theme.white,
    textAlign: "center",
    marginBottom: 10,
    zIndex: 999,
  },
  time: {
    //fontFamily: "roboto-regular",
    color: theme.primary,
    fontSize: 12,
    zIndex: 999,
  },
  iconTime: {
    color: theme.primary,
    fontSize: 13,
    height: 14,
    width: 13,
    zIndex: 999,
  },
  iconEnv: {
    //fontFamily: "quicksand-700",
    color: theme.primary,
    fontSize: 12,
    zIndex: 999,
  },
  km: {
    //fontFamily: "roboto-regular",
    color: theme.primary,
    fontSize: 12,
    zIndex: 999,
  },
  note: {
    //fontFamily: "roboto-regular",
    color: theme.primary,
    fontSize: 12,
    zIndex: 999,
  },
  noteCircle: {
    width: 8,
    height: 8,
    marginRight: 5,
    zIndex: 999,
  }
});

export default function MapDetail() {
  const navigation = useNavigation();
  const route = useRoute();
  return <MapDetailComponent navigation={navigation} route={route}></MapDetailComponent>
}
