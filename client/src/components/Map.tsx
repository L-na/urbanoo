import React, { Component } from 'react';
import { View, ImageURISource, StyleSheet, TouchableOpacity, Dimensions, Text } from 'react-native';
import MapView, { LatLng, Marker, Polyline } from 'react-native-maps';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import { API_MAP_DIRECTION } from '../utils/config';
import theme from '../Theme';
// Get services
import { Journey } from '../services/journey';
import { RouteProp } from '@react-navigation/native';
import { MainContext } from '../context/MainContext';
import { getPointsToDisplay } from '../utils/maps';

interface IProps {
  route: RouteProp<any, any>
}

interface IState {
  initRegion: any;
  region: any;
  latitude?: number;
  longitude?: number;
  errorMessage?: string;
  waypoints: LatLng[];
  polyline: LatLng[];
  isRecenterShown: Boolean;
}

const WIDTH = Dimensions.get('window').width;
export default class Map extends Component<IProps, IState> {
  static contextType = MainContext;

  constructor(props: IProps) {
    super(props);

    this.state = {
      initRegion: {
        latitude: 48.8523577,
        longitude: 2.3482825,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
      region: {
        latitude: 48.8523577,
        longitude: 2.3482825,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
      latitude: 48,
      longitude: 2,
      errorMessage: "Loading current location...",
      polyline: [],
      waypoints: [],
      isRecenterShown: false
    };

    this.onRegionChange = this.onRegionChange.bind(this);
    this.reCenter = this.reCenter.bind(this);
  }

  /**
   * Get journey and user data when component did mount
   */

  async componentDidMount() {
    const journey = await this.getJourney();

    const firstPlaceLocation = this.getFirstPlaceLocation(journey);
    const latitude = (undefined == firstPlaceLocation) ? this.state.latitude : firstPlaceLocation.lat;
    const longitude = (undefined == firstPlaceLocation) ? this.state.longitude : firstPlaceLocation.lng;

    const currentLocation = await this.getCurrentLocation()
    const pointToDisplay = await getPointsToDisplay(journey);
  
    this.setState({
      initRegion: {
        ...this.state.initRegion,
        latitude: latitude,
        longitude: longitude
      },
      ...currentLocation,
      ...pointToDisplay
    });
  }

  getJourney = async () => {
    const journeyId = this.props.route?.params?.data?.id;
    let journey;

    if (journeyId) {
      const journeyResponse = await Journey.one(journeyId);
      if (journeyResponse && journeyResponse.status === 200 && journeyResponse.data) {
        journey = journeyResponse.data
      } else {
        this.context.showToast("No journey found, please try again later");
      }
    } else {
      this.context.showToast("Missing journey id");
    }

    return journey;
  }


  // Get the position of the region of the journey
  getFirstPlaceLocation = (journey: any) => {
    let place;
    if (journey.steps && journey.steps.length > 0) {
      place = journey.steps[0].place
    }
    return place;
  }

  /**
   * Retrieve user current location
   */
  getCurrentLocation = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }

    let location = await Location.getCurrentPositionAsync({});

    return {
      latitude: location.coords.latitude,
      longitude: location.coords.longitude
    };
  };

  /**
   * Retrieve journey
   * Use pre-made journey for dev purposes
   */
  getJourney = async (journeyId?: Number): Promise<void> => {

    /**
     * Prepare itinerary and waypoints
     */
    let polyline: LatLng[] = []
    let waypoints: LatLng[] = []

    try {
      /**
       * Get pre-made journey data
       */
      const journeyId = this.props.route?.params?.data?.id;
      if (!journeyId) return;
      const journeyResponse = await Journey.one(journeyId);
      if (!journeyResponse || journeyResponse.status !== 200 || !journeyResponse.data) return;
      const steps = journeyResponse.data.steps;
      // const place = await Place.getPlaceByPosition(
      //   this.state.region.latitude,
      //   this.state.region.longitude,
      //   this.state.region.latitudeDelta,
      //   this.state.region.longitudeDelta,
      // );
      // const result = place;

      let url: string         = "https://maps.googleapis.com/maps/api/directions/json?origin=";
      let origin: string      = "";
      let waypoint: string    = "";
      let destination: string = "";
      if (steps && steps.length > 0) {
        steps.forEach((step: any, index: number) => {
          const place = step.place;

          // The first item
          if (index == 0) {
            origin = `${place.lat},${place.lng}`;
          }

          // The way between the starting point and the end
          if (steps.length > 1  && index > 0 && index < (steps.length - 1)) {
            if (index != (steps.length - 2)) {
              waypoint += `${place.lat},${place.lng}|`;
            } else {
              waypoint += `${place.lat},${place.lng}`;
            }
          }

          // The last item
          if ((steps.length - 1) == index) {
            destination = `destination=${place.lat},${place.lng}`;
            url += `${origin}${(waypoint.length>0) ? "&waypoints=" + waypoint : ""}&${destination}&language=fr&mode=walking&key=${API_MAP_DIRECTION}`
          }
        })
      }

      let tripDirections=null;
      if (url.includes("language=fr&mode=walking")) {
        let defaultJourney = await fetch(url);
        let defaultJourneyResponse = await defaultJourney.json();
        tripDirections = (defaultJourneyResponse.routes.length > 0) ? defaultJourneyResponse.routes[0].legs : [];
      }

      /**
       * Create the itinerary coordinates
       */
      tripDirections.forEach((leg: any, index: Number) => {

        /**
         * Create the waypoints coordinates
         */
        if(index == 0) {
          waypoints.push({
            latitude: leg.start_location.lat,
            longitude: leg.start_location.lng
          })
          waypoints.push({
            latitude: leg.end_location.lat,
            longitude: leg.end_location.lng
          })
        } else {
          waypoints.push({
            latitude: leg.end_location.lat,
            longitude: leg.end_location.lng
          })
        }

        /**
         * Create the polyline coordinate to draw the itinerary
         */
        polyline.push({
          latitude: leg.start_location.lat,
          longitude: leg.start_location.lng
        })

        leg.steps.forEach((step: any) => {
          polyline.push({
            latitude: step.start_location.lat,
            longitude: step.start_location.lng
          })
          polyline.push({
            latitude: step.end_location.lat,
            longitude: step.end_location.lng
          })
        })

        polyline.push({
          latitude: leg.end_location.lat,
          longitude: leg.end_location.lng
        })
      })
      /**
       * Update state
       */
      this.setState({
        polyline: polyline,
        waypoints: waypoints
      });
    } catch (error) {
      this.context.showToast("Fail getJourney"); // TODO improve this function
    }
  }

  onRegionChange(region: any) {
    this.setState(oldState => ({
      region,
      isRecenterShown: ((region.latitude + region.latitudeDelta) < oldState.initRegion.latitude ||
      (region.longitude + region.longitudeDelta) < oldState.initRegion.longitude ||
      region.latitude > (oldState.initRegion.latitude + oldState.initRegion.latitudeDelta) ||
      region.longitude > (oldState.initRegion.longitude + oldState.initRegion.longitudeDelta))}));
  }


  reCenter() {
    // TODO: get current position of the user
    this.setState(oldState => ({region: {
      ...oldState.region,
      latitude: oldState.initRegion.latitude,
      longitude: oldState.initRegion.longitude
    }}));
  }

  /**
   * Render component
   */
  render() {
    let marker
    let waypoints: any = []
    let latitude
    let longitude

    if (this.state.latitude && this.state.longitude) {
      latitude = this.state.latitude
      longitude = this.state.longitude

      /**
       * Retrieve icons for journeys
       */
      let iconStart: ImageURISource = require('../assets/marker-start.png')
      let iconBase: ImageURISource = require('../assets/marker-waypoint.png')
      let iconEnd: ImageURISource = require('../assets/marker-end.png')

      /**
       * Create markers for journey
       */
      this.state.waypoints.forEach((element, key) => {
        let icon = iconBase
        let iconOffset = {x: 0.5, y: 0.5}

        if (key == 0) {
          icon = iconStart
          iconOffset.y = 0.4
        } else if (key == this.state.waypoints.length - 1) {
          icon = iconEnd
          iconOffset.y = 1
        }
        waypoints.push(
          <Marker
            anchor={iconOffset}
            centerOffset={iconOffset}
            icon={(key == this.state.waypoints.length - 1) ? iconEnd : icon}
            pinColor={'orange'}
            key={key}
            coordinate={{
              latitude: element.latitude,
              longitude: element.longitude}}
            title={`Etape num ${key}`}>
          </Marker>
        )
      })

      marker = <Marker
        coordinate={{
          latitude: this.state.latitude,
          longitude: this.state.longitude
        }}
        title="You're here">
      </Marker>

    } else {
      // New York {37.78825, -122.4324}
      latitude = 100
      longitude = 100
    }

    return (
      <View style={Styles.containerMap}>
        <MapView
          style={Styles.map}
          region={this.state.region}
          initialRegion={this.state.initRegion}
          onRegionChangeComplete={this.onRegionChange}
        >
          { waypoints }
          {marker ? marker : null}
          <Polyline
            strokeColor={theme.brown}
            strokeWidth={4}
            coordinates={this.state.polyline}
            ></Polyline>
        </MapView>
        {/* {
          this.state.isRecenterShown && 
            <View style={Styles.containerButton}>
              <TouchableOpacity onPress={() => this.reCenter()} style={Styles.button}>
                <Text style={Styles.textButton}>Re center</Text>
              </TouchableOpacity>
            </View>
          } */}
      </View>
    )
  }
}

const Styles = StyleSheet.create({
	containerMap: {
    flex: 1,
    top: 0,
    zIndex: -1
	},
	map: {
    width: WIDTH,
    height: 200,
    flex: 1
  },
  containerButton: {
    alignItems: "center",
    position: "absolute",
    bottom: 35,
    right: 15,
    marginBottom: 20
  },
  button: {
    padding: 15,
    backgroundColor: "#B67B54",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center"
  },
  textButton: {
    color: "#FFFFFE",
    fontWeight: "bold"
  }
});