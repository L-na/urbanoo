import React from "react";
import { StyleSheet, View, Text } from "react-native";
import Svg, { Ellipse } from "react-native-svg";
import theme from "../Theme";

function NavStepAll() {
  return (
    <View style={styles.container}>
      <View style={styles.rect2}>
        <View style={styles.loremIpsumStackRow}>
          <View style={styles.loremIpsumStack}>
            <Text style={styles.loremIpsum}>1</Text>
            <Svg viewBox="0 0 21.76 21.76" style={styles.ellipse}>
              <Ellipse
                stroke={theme.primary}
                strokeWidth={2}
                cx={11}
                cy={11}
                rx={10}
                ry={10}
              ></Ellipse>
            </Svg>
          </View>
          <Text style={styles.etape13}>Etape 1</Text>
        </View>
        <View style={styles.rect3}></View>
        <View style={styles.loremIpsum1StackRow}>
          <View style={styles.loremIpsum1Stack}>
            <Text style={styles.loremIpsum1}>1</Text>
            <Svg viewBox="0 0 21.76 21.76" style={styles.ellipse1}>
              <Ellipse
                stroke={theme.primary}
                strokeWidth={2}
                cx={11}
                cy={11}
                rx={10}
                ry={10}
              ></Ellipse>
            </Svg>
          </View>
          <Text style={styles.etape14}>Etape 1</Text>
        </View>
        <View style={styles.rect4}></View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '80%',
    height: 494
  },
  rect2: {
    width: '100%',
    height: 494,
    backgroundColor: theme.white,
    borderTopLeftRadius: 25
  },
  loremIpsum: {
    top: 3,
    left: 7,
    position: "absolute",
    //fontFamily: "roboto-300",
    color: "rgba(89,74,78,1)"
  },
  ellipse: {
    top: 0,
    left: 0,
    width: 22,
    height: 22,
    position: "absolute"
  },
  loremIpsumStack: {
    width: 22,
    height: 22
  },
  etape13: {
    //fontFamily: "roboto-700",
    color: theme.brown,
    marginLeft: 21,
    marginTop: 3
  },
  loremIpsumStackRow: {
    height: 22,
    flexDirection: "row",
    marginTop: 24,
    marginLeft: 40,
    marginRight: 220
  },
  rect3: {
    width: '100%',
    height: 1,
    backgroundColor: "#E6E6E6",
    marginTop: 19
  },
  loremIpsum1: {
    top: 3,
    left: 7,
    position: "absolute",
    //fontFamily: "roboto-300",
    color: "rgba(89,74,78,1)"
  },
  ellipse1: {
    top: 0,
    left: 0,
    width: 22,
    height: 22,
    position: "absolute"
  },
  loremIpsum1Stack: {
    width: 22,
    height: 22
  },
  etape14: {
    //fontFamily: "roboto-700",
    color: theme.brown,
    marginLeft: 21,
    marginTop: 3
  },
  loremIpsum1StackRow: {
    height: 22,
    flexDirection: "row",
    marginTop: 14,
    marginLeft: 40,
    marginRight: 220
  },
  rect4: {
    width: '100%',
    height: 1,
    backgroundColor: "#E6E6E6",
    marginTop: 18
  },
});

export default NavStepAll;
