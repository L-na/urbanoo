import React, { Component } from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/FontAwesome";
import { NavigationProp } from '@react-navigation/native';
import Responsive from '../Responsive';
import { Helpers } from '../utils/helpers';
import theme from '../Theme';

interface Props {
  data: any,
  dataLength: Number,
  index: Number,
  navigation: NavigationProp<any>
}

interface State {
  is_hover: Boolean,
  duration?: String
}

class LastCircuit extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      is_hover: false,
      duration: null,
    }
  }

  componentDidMount() {
    const toMin = Helpers.millisToMinutesAndSeconds(this.props.data.duration)

    this.setState({
      duration: toMin,
    })
  }

  handleColorChange = () => {
    this.setState({
      is_hover: true,
    }, () => {
      setTimeout(() => {
        this.props.navigation.navigate("CircuitInfos", {
          data: this.props.data
        });
        this.setState({
          is_hover: false,
        })
      }, 50);
    });
  }

  render() {
    let { is_hover, duration } = this.state;

    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.handleColorChange}>
          <View style={[
            styles.card, 
            {backgroundColor: (false == is_hover) ? theme.white : theme.primaryBlue }, 
            {borderTopLeftRadius: ( 0 == this.props.index ) ? 15 : 0},
            {borderTopRightRadius: ( 0 == this.props.index ) ? 15 : 0},
            {borderBottomLeftRadius: ( (this.props.dataLength - 1) == this.props.index ) ? 15 : 0},
            {borderBottomRightRadius: ( (this.props.dataLength - 1) == this.props.index ) ? 15 : 0},
          ]}>
            <View style={styles.imageColumn}>
              <Image
                source={require("../assets/images/img.png")}
                resizeMode="contain"
                style={styles.imageCircuit}
              ></Image>
            </View>
            <View style={styles.aboutColumn}>
              <View style={styles.textColumn}>
                <Text style={[styles.title, {color: (false == is_hover) ? theme.dark : theme.white }]}>
                  {this.props.data.name}
                </Text>
                <Text style={[styles.title, {color: (false == is_hover) ? theme.dark : theme.white }]}>
                  La durée : {(null == duration) ? "" : duration}
                </Text>
              </View>
              <View style={styles.viewContainer}>
                <Icon
                  name="eye" style={[styles.view, {color: (false == is_hover) ? theme.green : theme.lightGreen }]}
                ></Icon>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: Responsive.width(90),
    marginBottom: 1,
  },
  card: {
    width: Responsive.width(90),
    height: 90,
    flexDirection: "row"
  },
  imageCircuit: {
    width: 50,
    height: 50,
    borderRadius: 5
  },
  title: {
    //fontFamily: "roboto-regular",
    fontSize: 12,
  },
  textColumn: {
    flex: 0.80,
  },
  viewContainer: {
    flex: 0.20,
    alignSelf: 'stretch',
    display: 'flex',
    justifyContent: 'center',
  },
  view: {
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'center',
  },
  imageColumn: {
    alignSelf: 'center',
    flexDirection: 'row',
    marginRight: 18,
    marginLeft: 18
  },
  aboutColumn: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexGrow: 1
  }
});

export default LastCircuit;
