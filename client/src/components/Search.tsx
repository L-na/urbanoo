import React from "react";
import { StyleSheet, View, Platform } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { GooglePlacesAutocomplete, GooglePlacesAutocompleteRef } from 'react-native-google-places-autocomplete';
import Responsive from '../Responsive'
import { Journey } from "../services/journey";
import { API_MAP_DIRECTION } from '../utils/config';
import { MainContext } from "../context/MainContext";
import theme from '../Theme'

interface Props {
  updateAction: Function
}

interface State {
  showClearButton: Boolean
}

class Search extends React.Component<Props, State>{
  ref: React.RefObject<GooglePlacesAutocompleteRef>;
  static contextType = MainContext;
  
  constructor(props: Props) {
    super(props);
    this.ref = React.createRef();
    this.state = {
      showClearButton: false
    }
  }

  clearAddressInput = () => {
    this.ref.current?.clear()
  }

  toggleClearButton = () => {
    this.setState({
      showClearButton: !this.state.showClearButton
    })
  }

  render() {
    return (
      <View style={styles.searchContainer}>
        <View style={[styles.iconContainer, styles.iconLeft]}>
          <Icon name="search" style={styles.icon}></Icon>
        </View>
        <View style={styles.search}>
          <GooglePlacesAutocomplete
            ref={this.ref}
            keyboardShouldPersistTaps='always'
            minLength={3}
            onFail={(error) => { this.context.showToast("Fail loading google places options") }}
            onNotFound={() => { this.context.showToast("No place found") }}
            enablePoweredByContainer={false}
            suppressDefaultStyles={true}
            styles={styles}
            placeholder='Rechercher...'
            fetchDetails={true}
            onPress={async (data) => {
              // Fetch journeys with place id provided by the user
              // To test on mobile, visit https://link.medium.com/YYyPwkJJhbb
              try {
                let journeysResponse = await Journey.allByPlaceId(data.place_id);
                // Update parent component journey list
                if (journeysResponse && journeysResponse.status === 200 && journeysResponse.data) {
                  this.props.updateAction(journeysResponse.data);
                } else {
                  this.props.updateAction([{'error': 'Aucun résultat pour le lieu recherché.'}]);
                }
              } catch (e) {
                this.context.showToast("Unable to fetch the corresponding places")
              }
            }}
            query={{
              key: API_MAP_DIRECTION,
              language: 'fr',
            }}
            textInputProps={{
              onFocus: () => {
                this.toggleClearButton()
              },
              onEndEditing: () => {
                this.toggleClearButton()
              }
            }}
          />
        </View>
        {this.state.showClearButton && Platform.OS != 'ios' &&
          <View style={[styles.iconContainer, styles.iconRight]}>
            <Icon name="times" style={styles.icon} onPress={() => {
              this.clearAddressInput()
            }}></Icon>
          </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  searchContainer: {
    zIndex: 1,
    position: 'absolute',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    right: 0,
    left: 0,
    top: Responsive.height(13)
  },
  search: {
    display: 'flex',
    flexDirection: 'row',
    alignSelf: 'stretch',
    position: 'relative',
    flexGrow: 1,
    zIndex: 1,
  },
  container: {
    width: Responsive.width(90),
    marginLeft: Responsive.width(5)
  },
  textInputContainer: {
    flexDirection: 'row',
    borderRadius: 10,
    backgroundColor: theme.white,
    elevation: 10,
    shadowOpacity: 0.3,
    shadowRadius: 30,
    shadowColor: theme.black,
    shadowOffset: { height: 3, width: 3 },
  },
  textInput: {
    height: 50,
    paddingVertical: 5,
    paddingLeft: 45,
    paddingRight: 50,
    fontSize: 15,
    flex: 1,
  },
  listView: {
    borderTopWidth: 15,
    borderTopColor: theme.white,
    marginTop: -10
  },
  row: {
    backgroundColor: theme.white,
    padding: 15,
    flexDirection: 'row',
  },
  separator: {
    height: 0.5,
    backgroundColor: '#c8c7cc',
  },
  description: {},
  loader: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    height: 20,
  },
  iconContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    height: 50,
    zIndex: 99,
    borderRadius: 8
  },
  iconRight: {
    paddingHorizontal: 20,
    right: Responsive.width(5),
  },
  iconLeft: {
    left: Responsive.width(5) + 15,
  },
  icon: {
    color: theme.primary,
    fontSize: 16,
    lineHeight: 16
  },
});

export default Search;