import React from "react";
import { StyleSheet, View, Text} from "react-native";
import Svg, { Ellipse } from "react-native-svg";
import Icon from "react-native-vector-icons/FontAwesome";
import { useNavigation } from '@react-navigation/native';
import { Dimensions } from "react-native";
import theme from '../Theme';

function Header(props: { name: React.ReactNode }) {
  const navigation = useNavigation();
  return (
    <View style={styles.headerStack}>
      <View style={styles.ellipseStack}>
        <Svg viewBox="0 0 815.59 247.03" style={styles.ellipse}>
          <Ellipse
            stroke="rgba(230, 230, 230,1)"
            strokeWidth={0}
            fill={theme.primary}
            cx={408}
            cy={124}
            rx={508}
            ry={124}
          ></Ellipse>
        </Svg>
      </View>
      <View style={styles.textStack}>
        <Text style={styles.textTitle}>{props.name}</Text>
      </View>
      <View style={styles.settingStack}>
        <Icon name="cogs" style={styles.iconSetting}></Icon>
      </View>
      <View style={styles.backStack}>
        {(props.name != "Home") ?
          <Icon onPress={() => navigation.goBack()} name="arrow-left" style={styles.iconBack}></Icon>
          : <View></View>
        }
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  headerStack: {
    width: Dimensions.get('window').width,
    height: '16%',
  },
  ellipseStack: {
    height: 247,
    marginTop: -124,
    marginLeft: -228,
    alignItems: 'center'
  },
  ellipse: {
    top: 0,
    width: 816,
    height: 275,
    position: "absolute",
    left: 0
  },
  textStack: {
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    right: 0,
    left: 0,
    top: 0
  },
  textTitle: {
    //fontFamily: "Quicksand_Bold",
    color: "#FFFFFE",
    fontSize: 30
  },
  settingStack: {
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    right: 0,
    top: 0,
    marginRight: 20
  },
  iconSetting: {
    fontSize: 40,
    color: theme.white
  },
  backStack: {
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    left: 0,
    top: 0,
    marginLeft: 20
  },
  iconBack: {
    color: theme.white,
    fontSize: 40
  },
});

export default Header;
