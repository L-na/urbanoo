import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faMicrophoneAlt } from '@fortawesome/free-solid-svg-icons'
import theme from '../Theme'

function NavStepDetails() {
  return (
    <View style={styles.container}>
      <View style={styles.rect2}>
        <Text style={styles.titreDeLetape}>Titre de l&#39;etape</Text>
        <View style={styles.blockVocall}>
          <FontAwesomeIcon size={ 21 } color={theme.primary} icon={ faMicrophoneAlt } />
          <Text style={styles.presentation}>Presentation</Text>
        </View>
        
        <Text style={styles.textExplication}>Text explication</Text>
        <Image
          source={require("../assets/images/imageEx.png")}
          resizeMode="contain"
          style={styles.image}
        ></Image>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 497
  },
  rect2: {
    width: '100%',
    height: 497,
    backgroundColor: theme.white,
    borderRadius: 25,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    display: 'flex',
    alignItems: 'center'
  },
  titreDeLetape: {
    //fontFamily: "roboto-700",
    color: theme.black,
    height: 'auto',
    width: '70%',
    textAlign: "center",
    fontSize: 12,
    marginTop: 26,
    marginBottom: 26,
  },
  blockVocall: {
    width: '70%',
    display: 'flex',
    flexDirection: 'row'
  },
  presentation: {
    //fontFamily: "roboto-700",
    color: theme.secondary,
    height: 'auto',
    fontSize: 11,
    marginTop: 4,
  },
  textExplication: {
    //fontFamily: "roboto-regular",
    color: theme.black,
    height: 'auto',
    width: '70%',
    fontSize: 12,
    marginTop: 26,
  },
  image: {
    width: 316,
    height: 137,
    marginTop: 30,
  }
});

export default NavStepDetails;
