import React from "react";
import { StyleSheet, TouchableOpacity, Text, View } from "react-native";
import { useNavigation } from '@react-navigation/native';
import theme from '../Theme';

function Button(props: { routeName: any; name: React.ReactNode; }) {
  const navigation = useNavigation();

  return (
    <View style={styles.containerButton}>
      <TouchableOpacity onPress={() => navigation.navigate(props.routeName)} style={styles.button}>
        <Text style={styles.textButton}>{props.name}</Text>
      </TouchableOpacity>
    </View>
    
  );
}

const styles = StyleSheet.create({
  containerButton: {
    alignItems: "center",
    position: "absolute",
    bottom: 0,
    right: 0,
    left: 0,
    marginBottom: 20
  },
  button: {
    width: 302,
    height: 56,
    backgroundColor: theme.secondary,
    borderWidth: 0,
    borderColor: theme.black,
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center"
  },
  textButton: {
    color: theme.white,
    fontWeight: "bold"
  }
});

export default Button;
