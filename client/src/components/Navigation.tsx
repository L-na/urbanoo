import React, { Component } from "react"
import { StyleSheet, TouchableHighlight, View } from "react-native"
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faShoePrints, faGlobeEurope, faStream } from '@fortawesome/free-solid-svg-icons'
import { Dimensions } from "react-native";
import NavStepAll from "../components/NavStepAll";
import NavStepDetails from "../components/NavStepDetails";
import { RouteProp } from '@react-navigation/native';
import theme from '../Theme';

interface IProps {
  route: RouteProp<any, any>;
}

interface IState {
  isHidden: Boolean;
  isHidden2: Boolean;
  hide: Boolean;
}
export default class Navigation extends Component<IProps, IState> {
// function Navigation() {
  constructor(props: IProps) {
    super(props)
    this.state = {
      isHidden: false,
      isHidden2 : false,
      hide : false
    }
  }

  componentHideAndShow = (e: boolean) => {
    if (e) {
      this.setState(previousState => ({ isHidden: !previousState.isHidden, isHidden2: false, hide: e }))
    }
    else{
      this.setState(previousState => ({ isHidden2: !previousState.isHidden2, isHidden: false, hide: e }))
    }
  }
  
  componentHide = () => {
    this.setState(previousState => ({ isHidden: !previousState.isHidden, isHidden2: !previousState.isHidden2 }));
  }

  render() {
    return (
      <View style={styles.navContainer}>
        <View>
          <View style={styles.navStepAll}>
            {
              this.state.isHidden2 ?
              <NavStepAll></NavStepAll>
              :null
            }
          </View>
          <View>
            {
              this.state.isHidden ?
              <NavStepDetails></NavStepDetails>
              :null
            }
          </View>
        </View>

        <View style={styles.rect}>
          <View style={styles.imageRow}>
            <TouchableHighlight onPress={() => this.componentHideAndShow(true)} underlayColor="white">
              <View >
                <FontAwesomeIcon size={ 27 } color={theme.primary} icon={ faShoePrints } />
              </View>
            </TouchableHighlight>

            <TouchableHighlight onPress={() => this.componentHide()} underlayColor="white">
              <View >
                <FontAwesomeIcon size={ 27 } color={theme.primary} icon={ faGlobeEurope } />
              </View>
            </TouchableHighlight>
            
            <TouchableHighlight onPress={() => this.componentHideAndShow(false)} underlayColor="white">
              <View >
                <FontAwesomeIcon size={ 27 } color={theme.primary} icon={ faStream } />
              </View>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navContainer: {
    height: '84%',
    position: 'absolute',
    bottom: 0,
    display: 'flex',
    justifyContent: 'flex-end'
  },
  navStepAll: {
    display: 'flex',
    alignItems: 'flex-end'
  },
  rect: {
    width: Dimensions.get('window').width,
    height: 50,
    backgroundColor: theme.white,
    shadowColor: theme.black,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowOpacity: 1,
    shadowRadius: 10,
  },
  imageRow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: 'center'
  }
});

