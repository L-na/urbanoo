import React from "react";
import { StyleSheet, View, Text } from "react-native";
import theme from '../Theme';

function ButtonStep(props: { number: React.ReactNode; }) {
  return (
    <View style={styles.container}>
      <View style={styles.rect2}>
        <Text style={styles.etape1}>ETAPE {props.number}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 120,
    height: 30
  },
  rect2: {
    width: 120,
    height: 30,
    backgroundColor: theme.secondary,
    borderWidth: 0,
    borderColor: theme.black,
    borderRadius: 8
  },
  etape1: {
    //fontFamily: "Roboto-Bold",
    color: theme.white,
    marginTop: 7,
    marginLeft: 34
  }
});

export default ButtonStep;
