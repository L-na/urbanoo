import { Dimensions } from 'react-native'

const window = Dimensions.get("window")
const screen = Dimensions.get("screen")

/**
 * Create responsive dimensions from screen or window values
 */
const Responsive = {
    width: (percentage: number = 100, useWindow: Boolean = false) => {
        let dimension = (useWindow) ? window : screen
        return dimension.width * percentage / 100
    },
    height: (percentage: number = 100, useWindow: Boolean = false) => {
        let dimension = (useWindow) ? window : screen
        return dimension.height * percentage / 100
    },
    fontScale: (percentage: number = 100, useWindow: Boolean = false) => {
        let dimension = (useWindow) ? window : screen
        return dimension.fontScale * percentage / 100
    },
    scale: (percentage: number = 100, useWindow: Boolean = false) => {
        let dimension = (useWindow) ? window : screen
        return dimension.scale * percentage / 100
    }
}

export default Responsive