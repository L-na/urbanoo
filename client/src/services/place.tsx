import axios from 'axios';
import { API_HOST } from '../utils/config';

export const Place = {
	all: async () => {
		const url = `${API_HOST}place`;
		return await axios.get(url);
	},
	getPlaceByPosition: async (latitude: any, longitude: any, latitudeDelta: any, longitudeDelta: any) => {
		const url = `${API_HOST}place?latitude=${latitude}&longitude=${longitude}&latitudeDelta=${latitudeDelta}&longitudeDelta=${longitudeDelta}`;
		return await axios.get(url);
	},
	one: async (id: Number) => {
		const url   = `${API_HOST}place/${id}`;
		return await axios.get(url);
	},
	create: async (googlePlaceId: String, lat: Number, lng: Number, title: String, description: String, picture: String, category: String) => {
		const place = {
			googlePlaceId: googlePlaceId,
			lat: lat,
			lng: lng,
			title: title,
			description: description,
			picture: picture,
			category: category
		}
		const url = `${API_HOST}place`;
		return await axios.post(url, {place});
	},
	edit: async (id: Number, googlePlaceId: String, lat: Number, lng: Number, title: String, description: String, picture: String, category: String) => {
		const place = {
			googlePlaceId: googlePlaceId,
			lat: lat,
			lng: lng,
			title: title,
			description: description,
			picture: picture,
			category: category
		}
		const url = `${API_HOST}place/${id}`;
		return await axios.put(url, {place});
	},
	delete: async (id: Number) => {
		const url = `${API_HOST}place/${id}`;
		return await axios.delete(url);
	}
}