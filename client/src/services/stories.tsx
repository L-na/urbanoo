import axios from 'axios';
import { API_HOST } from '../utils/config';

export const Stories = {
	all: async () => {
		const url = `${API_HOST}stories`;
		return await axios.get(url);
	},
	one: async (id: Number) => {
		const url   = `${API_HOST}stories/${id}`;
		return await axios.get(url);
	},
	create: async (name: String, description: String) => {
		const story = {
			name: name,
			description: description
		}
		const url = `${API_HOST}stories`;
		return await axios.post(url, {story});
	},
	edit: async (id: Number, name: String, description: String) => {
		const story = {
			name: name,
			description: description
		}
		const url = `${API_HOST}stories/${id}`;
		return await axios.put(url, {story});
	},
	delete: async (id: Number) => {
		const url = `${API_HOST}stories/${id}`;
		return await axios.delete(url);
	}
}