import axios from 'axios';
import { API_HOST } from '../utils/config';

export const Journey = {
	all: async () => {
		const url = `${API_HOST}journey`;
		return await axios.get(url);
	},
	one: async (id: Number) => {
		const url = `${API_HOST}journey/${id}`;
		return await axios.get(url);
    },
    allByPlaceId: async(googlePlaceId: String) => {
        const url = `${API_HOST}journey?googlePlaceId=${googlePlaceId}`;
        return await axios.get(url);
    },
	create: async (name: String, description: String, duration: Number, difficulty: Number, city: String) => {
		const journey = {
			name: name,
			description: description,
			duration: duration,
			difficulty: difficulty,
			city: city,
		}
		const url = `${API_HOST}journey`;
		return await axios.post(url, {journey});
	},
	edit: async (id: Number, name: String, description: String, duration: Number, difficulty: Number, city: String) => {
		const journey = {
			name: name,
			description: description,
			duration: duration,
			difficulty: difficulty,
			city: city,
		}
		const url = `${API_HOST}journey/${id}`;
		return await axios.put(url, { journey });
	},
	delete: async (id: Number) => {
		try {
			const url = `${API_HOST}journey/${id}`;
			return await axios.delete(url);
		} catch (err) {
			console.log(err)
		}
	},
	// Get the last five journey
	getLastFive: async () => {
		try {
			const url = `${API_HOST}journey/last-five`;
			return await axios.get(url);
		} catch (err) {
			console.log(err)
		}
	},
}