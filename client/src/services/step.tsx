import axios from 'axios';
import { API_HOST } from '../utils/config';

export const Step = {
	all: async () => {
		const url  = `${API_HOST}step`;
		return await axios.get(url);
	},
	one: async (id: Number) => {
		const url  = `${API_HOST}step/${id}`;
		return await axios.get(url);
	},
	create: async (number: Number, journeyId: Number, placeId: Number) => {
		const step = {
			number: number,
			journey: {
				id: journeyId
			},
			place: {
				id: placeId
			}
		}
		const url = `${API_HOST}step`;
		return await axios.post(url, {step});
	},
	edit: async (id: Number, number: Number, journeyId: Number, placeId: Number) => {
		const step = {
			number: number,
			journey: {
				id: journeyId
			},
			place: {
				id: placeId
			}
		}
		const url = `${API_HOST}step/${id}`;
		return await axios.put(url, {step});
	},
	delete: async (id: Number) => {
		const url = `${API_HOST}step/${id}`;
		return await axios.delete(url);
	}
}