export default {
    primary: 'rgb(97,166,78)',
    secondary: 'rgb(67,144,199)',
    black: 'rgb(73,60,50)',
    brown: 'rgb(198,135,81)',
    white: 'rgb(255,255,254)',
    primaryBlue: "rgb(67,144,199)",
    secondaryBlue: "rgb(82,153,203)",
    dark: "rgb(18,18,18)",
    green: "rgb(100,180,20)",
    lightGreen: "rgb(212,255,194)",
    popupGreen: "rgb(168,202,125)",
};