import { StyleSheet } from 'react-native';
import theme from './Theme';

const Styles = StyleSheet.create({
	container: {
		flex: 1,
	},
});

export default Styles;