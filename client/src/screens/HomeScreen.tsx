import React from "react";
import { StyleSheet, View, ImageBackground, Text } from "react-native";
import Header from "../components/Header";
import Button from "../components/Button";
import { Dimensions } from "react-native";
import Styles from "../Styles";

function HomeScreen() {
  return (
    <View style={Styles.container}>
      <Header name="Home"></Header>
      <View style={styles.containerImage}>
        <ImageBackground
          source={require("../assets/images/undraw_balloons.png")}
          resizeMode="contain"
          style={styles.image}
        >
          {/* <Text style={styles.text}>
            Vous n’avez pas fait encore de parcours.{"\n"}Commencez à chercher un
            parcours !
          </Text> */}
        </ImageBackground>
      </View>

      <Button style={styles.startButton} routeName="Research" name="CHERCHER UN PARCOURS"></Button>
    </View>
  );
}

const styles = StyleSheet.create({
  containerImage: {
    height: (Dimensions.get('window').height-199),
    alignItems: "center",
    justifyContent: "center"
  },
  image: {
    width: 350,
    height: 350,
  },
  text: {
    //fontFamily: "Roboto-Regular",
    textShadowOffset: {width: 0,height: 0},
    textShadowRadius: 7,
    textShadowColor:'white',
    color: "rgba(112,85,61,1)",
    marginTop: 159,
    marginLeft: 50
  }
});

export default HomeScreen;
