import React from "react"
import { View, StyleSheet } from "react-native";
import Map from "../components/Map"
import Header from "../components/Header";
import MapDetail from "../components/MapDetail"
import { useRoute } from "@react-navigation/native";
import Styles from "../Styles";

function MapScreen() {
  const route = useRoute();
  return (
    <View style={Styles.container}>
      <Header name="Parcours"></Header>
      <Map route={route}></Map>
      <MapDetail></MapDetail>
    </View>
  );
}

export default MapScreen;
