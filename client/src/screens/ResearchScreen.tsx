import React from "react";
import { View, FlatList, Text, ScrollView, StyleSheet, Dimensions, YellowBox, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/SimpleLineIcons";
import { LinearGradient } from 'expo-linear-gradient';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import Header from "../components/Header";
import Search from "../components/Search";
import LastCircuit from "../components/LastCircuit";
import { Journey } from "../services/journey";
import { MainContext } from "../context/MainContext";
import Theme from "../Theme";
import Styles from "../Styles";

// dev
YellowBox.ignoreWarnings(['VirtualizedLists should never be nested']);
interface Props {
  navigation: NavigationProp<any>
}

interface State {
  data: Array<Object | null>,
  dataSearched: Array<Object | null>,
  dataLength: Number,
  isLoadingJourneys: Boolean,
  searchMessage: String
}

const HEIGHT = Dimensions.get('window').height;
// Set the navigation props
class ResearchScreenComponent extends React.Component<Props, State> {
  static contextType = MainContext;
  /**
   * Constructor
   */
  constructor(props: Props) {
    super(props);
    this.state = {
      data: [],
      dataSearched: [],
      dataLength: 0,
      isLoadingJourneys: true,
      searchMessage: 'Recherche de parcours...',
    }
  }

  /**
   * Update journeys list
   */
  componentDidMount = async () => {
    const journeys: any = await this.getAllJourney();

    this.setState({
      dataLength: journeys.length,
    });

    if (journeys && journeys.length > 0) {
      journeys.map((item: any) => {
        this.setState({
          data: [...this.state.data, item],
          isLoadingJourneys: false
        }, () => {
          this.setState({
            isLoadingJourneys: true
          });
        })
      });
    } else {
      this.context.showToast("No journey found");
    }
  }

  getAllJourney = async () => {
    let journeys;
    try {
      const journeysResponse = await Journey.getLastFive();
      if (undefined != journeysResponse && journeysResponse.status === 200)
        journeys = journeysResponse.data;
    } catch (err) {
      this.context.showToast("Error occured when contacting the server");
    }
    return journeys;
  }

  randomJourney = (length) => {
    const index = Math.floor(Math.random() * length);
    return this.state.data[index];
  }

  // Get google data
  updateResults = async (data: Array<Object | null>) => {
    if ( data && data.length > 0 ) {
      this.setState({
        isLoadingJourneys: true,
        data: data
      });
    } else {
      this.setState({
        isLoadingJourneys: false,
        searchMessage: 'Aucun résultat pour le lieu recherché.'
      });
    }
  }

  renderRow = ({item, index}: {item: any, index: Number}) => {
    return <LastCircuit
      navigation={this.props.navigation}
      data={item}
      dataLength={this.state.dataLength}
      index={index}
    ></LastCircuit>
  }

  render() {
    let { data, isLoadingJourneys, searchMessage } = this.state;

    // tu clique sur un des 5 parcours -> rediriger vers la page du parcours avec toutes les infos
    // tu clique sur un parcours que tu as recherché ou icon map -> redirigé vers la map avec tous les parcours
    return (
      <View style={{ height: (HEIGHT-199), flex: 1 }}>
        <Header name="Research"></Header>
        <Search updateAction={this.updateResults}></Search>
        <ScrollView>
          { false != (isLoadingJourneys) ?
            <View style={[styles.flexContent, styles.flatlistStack, {marginTop: 50}]}>
              <Text style={[styles.textLast]}>Les derniers parcours</Text>
              {data && data.length > 0 ?
                <FlatList
                  style={styles.boxShadow}
                  data={data}
                  renderItem={this.renderRow}
                  keyExtractor={(item: any) => `${item.id}`}
                /> : <View></View>
              }
            </View> :
            <View style={[styles.flexContent, styles.loadingMessage, {marginTop: 200}]}>
              <Text style={{ textAlign: 'center' }}>
                {searchMessage}
              </Text>
            </View>
          }
        </ScrollView>
        {/* Map Icon */}
        
        <View style={{ alignItems: 'flex-end', flex:1, margin: 5}}>  
          <View style={styles.floatingMapButton}>
            <LinearGradient
              style={styles.mapStack}
              colors={["#4390C7", "#4390C7"]}
            >
              <Icon
                onPress={() => this.props.navigation.navigate("Map", {
                  data: this.randomJourney(data.length)
                })}
                name="map"
                style={styles.iconMap}
              ></Icon>
            </LinearGradient>
          </View>
        </View>
 
        
      </View>
    )
  }
}

const styles = StyleSheet.create({
  flexContent: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  boxShadow: {
    shadowColor: Theme.secondaryBlue,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 15,
  },
  textLast: {
    width: 315,
    fontSize: 14,
    textAlign: "left",
    marginBottom: 20
  },
  mapStack: {
    width: 75,
    height: 75,
    borderRadius: 50,
  },
  map: {
    width: 75,
    height: 75,
  },
  iconMap: {
    top: 18,
    left: 20,
    position: "absolute",
    color: "rgba(255,255,255,1)",
    fontSize: 35
  },
  loadingMessage: {
    textAlign: 'center',
  },
  startButton: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    justifyContent: 'center'
  },
  floatingMapButton: {
    position:'absolute',
    bottom:0,
    right: 0,
    marginRight: 15,
    marginBottom: 15,
  }
});

export default function ResearchScreen() {
  const navigation = useNavigation();
  return <ResearchScreenComponent navigation={navigation}></ResearchScreenComponent>
}