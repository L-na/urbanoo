import React from "react"
import { View } from "react-native";
import Map from "../components/Map"
import Header from "../components/Header";
import Navigation from "../components/Navigation"
import { RouteProp, useRoute } from "@react-navigation/native";
import Styles from "../Styles";

function MapScreen() {
  const route = useRoute();
  // TODO: replace name variable by the right value
  return (
    <View style={Styles.container}>
      <Header name="Parcours $Name"></Header>
      <Map route={route}></Map>
      <Navigation route={route}></Navigation>
    </View>
  );
}

export default MapScreen;
