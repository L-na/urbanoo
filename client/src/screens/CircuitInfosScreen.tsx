import React from "react"
import { View, StyleSheet, ImageBackground, Text, ScrollView } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import Map from "../components/Map";
import { Helpers } from '../utils/helpers';
import Header from "../components/Header";
import Button from "../components/Button";
import { 
  useRoute,
  RouteProp
} from '@react-navigation/native';

interface IProps {
  route: RouteProp<any, any>
}

interface IState {
  details: {
    city?: String,
    description?: String,
    difficulty?: Number,
    duration?: Number,
    id?: Number,
    name?: String,
  },
  duration?: String
}


class CircuitInfosComponent extends React.Component<IProps, IState> {

  constructor(props: IProps) {
    super(props);
    this.state = {
      details: {},
      duration: null,
    }
  }

  componentDidMount() {
    try {
      const data = this.props.route.params.data;
      if( null == data ) {
        return;
      }

      const toMin = Helpers.millisToMinutesAndSeconds(data.duration)
      this.setState({
        details: data,
        duration: toMin,
      });
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    let { details, duration } = this.state;

    return (
      <ScrollView style={styles.container}>
        {/* Header */}
        <Header name="Parcours Infos"></Header>
  
        {/* Body */}
        <View>
          {/* IMG */}
          <View>
            <ImageBackground
                source={require("../assets/images/imageEx.png")}
                resizeMode="cover"
                style={[styles.image]}
            ></ImageBackground>
          </View>
          {/* Content */}
          <View style={styles.content}>
            {/* Title */}
            <View>
              <Text style={styles.title}>{details.name}</Text>
            </View>
            {/* Info */}
            <View style={[styles.info, styles.flexContent]}>
              <Icon name="clock-o" style={styles.icon}></Icon>
              <Text style={styles.infoText}>
                {(null == duration) ? "" : duration}
              </Text>
              <Icon name="tachometer" style={[styles.icon, {marginLeft: 50}]}></Icon>
              <Text style={[styles.infoText]}>
                {details.difficulty}
              </Text>
            </View>
            {/* Description */}
            <View style={[styles.description, styles.flexContent]}>
              <Text>{details.description}</Text>
            </View>
          </View>
          {/* Map */}
          <View style={[styles.map, styles.flexContent]}>
           <Map  route={this.props.route}></Map>
          </View>
        </View>
  
        {/* Footer */}
        <View style={[styles.startButton, styles.flexContent]}>
          <Button routeName="Circuit" name="COMMENCER"></Button>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  icon: {
    marginRight: 5,
    color: "#64B414",
  },
  image: {
    width: "100%",
    height: 215,
    position: "absolute",
    zIndex: -1,
    overflow: "hidden",
    marginTop: -30
  },
  content: {
    marginTop: 235,
    marginLeft: 15,
  },
  title: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
  },
  flexContent: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  info: {
    marginTop: 15,
  },
  infoText: {
    color: "#64B414",
    textAlign: "justify",
  },
  description: {
    textAlign: "justify",
    marginTop: 15,
  },
  map: {
    marginTop: 25,
  },
  startButton: {
    marginTop: 100,
    marginBottom: 25,
  }
});

export default function CircuitInfosScreen() {
  const route = useRoute();
  return  <CircuitInfosComponent 
            route={route}
          ></CircuitInfosComponent>
}