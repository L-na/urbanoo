import { LatLng } from "react-native-maps";
import { API_MAP_DIRECTION } from "./config";

interface GPSPoint {
    lat: number;
    lng: number;
}

interface Path {
    start_location: GPSPoint;
    end_location: GPSPoint;
    steps: Array<Path>;
}

interface PointToDisplay {
    polyline: LatLng,
    waypoints: LatLng
}

const generateGoogleApiUrl = (steps: any) => {
    let url: string         = "https://maps.googleapis.com/maps/api/directions/json?origin=";
    let origin: string = "";
    let waypointsString: Array<string> = [];
    let destination: string = "";

    if (steps && steps.length > 0) {
      steps.forEach((step: any, index: number) => {
        const place = step.place;
 
        // The first item
        if (index == 0) {
          origin = `${place.lat},${place.lng}`;
        }
 
        // The way between the starting point and the end
        if (index > 0 && (index + 1) < steps.length && steps.length > 1) {
          waypointsString.push(`${place.lat},${place.lng}`);
        }
 
        // The last item
        if ((steps.length - 1) == index) {
          destination = `destination=${place.lat},${place.lng}`;
          url += `${origin}&${destination}&waypoints=${waypointsString.join('|')}&language=fr&mode=walking&key=${API_MAP_DIRECTION}`
        }
      })
    }
    if (url.includes("language=fr&mode=walking")) {
        return url;
    } else {
        return "";
    }
}

export const getPointsToDisplay = async (journey: any) => {
/**
 * Prepare itinerary and waypoints
 */
 let polyline: LatLng[] = []
 let waypoints: LatLng[] = []
   

   const url = generateGoogleApiUrl(journey.steps)

   const defaultJourney = await fetch(url);
   const defaultJourneyResponse = await defaultJourney.json();
   let tripDirections = defaultJourneyResponse.routes[0].legs;

   /**
    * Create the itinerary coordinates
    */
   tripDirections.forEach((leg: Path, index: Number) => {

     /**
      * Create the waypoints coordinates
      */
     if(index == 0) {
       waypoints.push({
         latitude: leg.start_location.lat,
         longitude: leg.start_location.lng
       })
       waypoints.push({
         latitude: leg.end_location.lat,
         longitude: leg.end_location.lng
       })
     } else {
       waypoints.push({
         latitude: leg.end_location.lat,
         longitude: leg.end_location.lng
       })
     }

     /**
      * Create the polyline coordinate to draw the itinerary
      */
     polyline.push({
       latitude: leg.start_location.lat,
       longitude: leg.start_location.lng
     })

     leg.steps.forEach((step: any) => {
       polyline.push({
         latitude: step.start_location.lat,
         longitude: step.start_location.lng
       })
       polyline.push({
         latitude: step.end_location.lat,
         longitude: step.end_location.lng
       })
     })

     polyline.push({
       latitude: leg.end_location.lat,
       longitude: leg.end_location.lng
     })
   })
   
   return {
    polyline,
    waypoints
    }
}
