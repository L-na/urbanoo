export const Helpers = {
    millisToMinutesAndSeconds: (millis) => {
		const minutes = Math.floor(millis / 60000);
        const seconds = ((millis % 60000) / 1000).toFixed(0);
    
        return minutes + "h" + (seconds < 10 ? '0' : '') + seconds;
	},
}