import { API_HOST as HOST, API_MAP_DIRECTION as DIRECTION } from '@env';

export  const API_HOST = HOST.length > 0 ? (HOST.charAt(HOST.length - 1) === "/" ? HOST : `${HOST}/`) : "";
export const API_MAP_DIRECTION = DIRECTION;